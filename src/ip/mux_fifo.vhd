library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.edacom_pkg.all;

-- pragma translate_off
library osvvm;
use osvvm.AlertLogPkg.all;
-- pragma translate_on

entity mux_fifo is
  generic (
    g_enabled_channels : std_logic_vector := X"FF";
    -- pragma translate_off
    mux_fifo_id        : alertlogidtype   := GetAlertLogID("mux-fifo");
    -- pragma translate_on
    g_extend           : natural range 0 to 1
    );
  port (
    rst        : in    std_logic;
    clk        : in    std_logic;
    -- fifo if
    fifo_rd_if : inout t_fifo_rd_if_array;
    -- out if
    dataout    : out   std_logic_vector;
    wr_en      : out   std_logic;
    full       : in    std_logic
    );
begin

  -- pragma translate_off
  process(clk) is
  begin
    if (clk'event and clk = '1') then
      if (wr_en) then
        Log(mux_fifo_id, "writting data " &
            to_string(to_integer(unsigned(dataout(dataout'left downto 8)))) &
            "." &
            to_hstring(dataout(7 downto 0)), DEBUG);
      end if;
    end if;
  end process;
  -- pragma translate_on

end entity mux_fifo;

architecture simple of mux_fifo is

  type t_state is (s_wait, s_capture, s_write);

  signal fifo_rd_if_rd    : std_logic_vector(fifo_rd_if'range) := (others => '0');
  signal fifo_rd_if_empty : std_logic_vector(fifo_rd_if'range) := (others => '0');
  signal state            : t_state;

begin

  u_clk : for i in fifo_rd_if'range generate
    fifo_rd_if(i).clk   <= clk;
    fifo_rd_if(i).rd    <= fifo_rd_if_rd(i);
    fifo_rd_if_empty(i) <= fifo_rd_if(i).empty;
  end generate u_clk;

  u_mux : process (clk) is

    variable index : integer := - 1;

  begin

    if (clk'event and clk = '1') then
      if (rst = '1') then
        index            := - 1;
        dataout          <= (others => '0');
        wr_en            <= '0';
        fifo_rd_if_rd <= (others => '0');
        state            <= s_wait;
      else

        case state is

          when s_wait =>

            index         := edacom_onehot_to_integer_neg(fifo_rd_if_empty);
            dataout       <= (others => '0');
            wr_en         <= '0';
            fifo_rd_if_rd <= (others => '0');
            if (index >= 0) then
              -- pragma translate_off
              Log(mux_fifo_id, "reading from non empty fifo n. " & to_string(index), DEBUG);
              -- pragma translate_on
              fifo_rd_if_rd(index) <= '1';
              state                <= s_capture;
            end if;

          when s_capture =>

            dataout          <= (others => '0');
            wr_en            <= '0';
            fifo_rd_if_rd <= (others => '0');
            if (not(full)) then
              state <= s_write;
            end if;

          when s_write =>

            fifo_rd_if_rd <= (others => '0');
            if (g_extend = 1) then
              dataout <= fifo_rd_if(index).data & std_logic_vector(to_signed(index, 8));
            else
              dataout <= fifo_rd_if(index).data;
            end if;
            wr_en         <= '1';
            fifo_rd_if_rd <= (others => '0');
            state         <= s_wait;

        end case;

      end if;
    end if;

  end process u_mux;

end architecture simple;
